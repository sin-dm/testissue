/*
No1
У тебя есть текстовый файл, в котором есть значения от 0 до 20 в произвольном порядке
(перемешаны). Значения указаны через запятую.
1. Распарси файл, отфильтруй значения по возрастанию и выведи на консоль.
2. Распарси файл, отфильтруй значения по убыванию и выведи на консоль.
*/


import java.io.*;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Scanner;

public class FirstTestIssue {

    public static void main(String[] args) throws IOException {
        Scanner scanner = new Scanner(new File("test.txt"));
        scanner.useDelimiter(",");
        ArrayList<Integer> list = new ArrayList<>();

        while (scanner.hasNext()) {
            list.add(Integer.parseInt(scanner.next()));
        }
        /* Первая часть задания: отсортировать  по возрастанию*/
        list.sort(Comparator.naturalOrder());
        for (Integer x : list) {
            System.out.println(x);
        }

        /* Вторая часть задания: отсортировать по убыванию */
        list.sort(Comparator.reverseOrder());
        for (Integer x : list) {
            System.out.println(x);
        }

        scanner.close();


    }

}
